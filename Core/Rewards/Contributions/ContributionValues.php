<?php
namespace Minds\Core\Rewards\Contributions;

class ContributionValues
{

    public static $multipliers = [
        'comments' => 2,
        'reminds' => 4,
        'votes' => 1,
        'subscribers' => 4,
        'referrals' => 50,
        'referrals_welcome' => 50,
        'checkin' => 2,
        'jury_duty' => 25,
    ];

}
